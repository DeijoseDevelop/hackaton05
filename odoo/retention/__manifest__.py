{
    "name": 'Retention module',
    "author": 'Jhonatan Alvarez, Deiver Vazquez',
    "version": '1.0.0-dev11',
    "summary": 'This module implements retention to invoices',
    "depends": ['account',],
    "data": [ 'security/user_groups.xml',
                'security/ir.model.access.csv',
                'views/res_partner_view.xml',
                'views/retention_view.xml',
                'views/account_move.xml',
    ],
}
