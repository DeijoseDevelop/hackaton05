"""
This is a python module used to modify the module account_move
"""

from email.policy import default
import logging
from odoo import models, fields, api


class AccountMove(models.Model):
    """
    This model is override to include, functionalities related to retentions module.
    """
    _name = "account.move"
    _inherit = "account.move"

    retention_total = fields.Monetary(
        string='Retention',
        readonly=True,
        store=True,
    )


    @api.depends(
        'line_ids.debit',
        'line_ids.credit',
        'line_ids.currency_id',
        'line_ids.amount_currency',
        'line_ids.amount_residual',
        'line_ids.amount_residual_currency',
        'line_ids.payment_id.state')

    def validate_retention(self):
        """
        This method is used to validate the retention.
        """
        for record in self:
            if record.partner_id and record.partner_id.retention_id.state_retention:
                if record.amount_total > record.partner_id.retention_id.min_retention:
                    record.retention_total = record.amount_untaxed * (record.partner_id.retention_id.value_retention / 100) * - 1
                    record.amount_total = record.amount_total + record.retention_total
                    record.amount_residual = record.amount_total
                    return

    def _compute_amount(self):

        super(AccountMove, self)._compute_amount()

        self.validate_retention()
