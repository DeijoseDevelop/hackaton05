
#esto lo estoy testeando

"""
This is a python module used to modify the module account_payment
"""

from email.policy import default
import logging
from odoo import models, fields, api


class AccountPayment(models.Model):
    """
    This model is override to include, functionalities related to retentions module.
    """
    _name = "account.payment"
    _inherit = "account.payment"

    amount = fields.Monetary(
        string='Amount', readonly=True, store=True)