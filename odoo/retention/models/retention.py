"""
This is a python module built to implement the retention odoo module
"""

from email.policy import default
from odoo import models, fields, models

class Retention(models.Model):
    """
    Model to manage the user's retentions
    """
    _name = "retention.retention"
    _description = "Manage the user retentions."
    _rec_name = "id"
    _order = "create_date"

    user_id = fields.Many2one('res.partner', string='user retention', store=True, readonly=True)
    state_retention = fields.Boolean(default=False, string='Aplica retención?')
    value_retention = fields.Float(string="Porcentaje de Retención.")
    min_retention = fields.Float(string="Valor aplicado retención.")

